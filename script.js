const cookieBox = document.querySelector(".wrapper"),
    buttons = document.querySelectorAll(".button");
const executeCodes = () => {
    cookieBox.classList.add("show");
    buttons.forEach((button) => {
        button.addEventListener("click", () => {
            cookieBox.classList.remove("show");

            if (button.id == "accept") {
                document.cookie = "cookieBy= Ilchenko; max-age=" + 60 * 60 * 24 * 30;
            }
        });
    });
};
window.addEventListener("load", executeCodes);